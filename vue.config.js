module.exports = {
  transpileDependencies: ["vuetify"],
  pwa: {
    name: "Mentio",
    themeColor: "#3f51b5",
    msTileColor: "#ffffff",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black"
  }
};
