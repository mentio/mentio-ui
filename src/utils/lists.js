/* eslint-disable no-console */

export default {
  chunk(list, numberOfChunks) {
    let chunkedList = [];

    let counter = 0;
    while (counter < numberOfChunks) {
      chunkedList[counter] = [];
      counter++;
    }

    let index = 0;
    let pointer = 0;
    while (index < list.length) {
      chunkedList[pointer].push(list[index]);
      pointer = (pointer + 1) % numberOfChunks;
      index++;
    }
    return chunkedList;
  },

  chunkByBreakpoint(list, breakpoint) {
    let chunkedList = [];
    switch (breakpoint.name) {
      case "xs":
        chunkedList = this.chunk(list, 1);
        break;
      case "sm":
        chunkedList = this.chunk(list, 2);
        break;
      case "md":
        chunkedList = this.chunk(list, 3);
        break;
      case "lg":
        chunkedList = this.chunk(list, 4);
        break;
      case "xl":
        chunkedList = this.chunk(list, 5);
        break;
      default:
        chunkedList = this.chunk(list, 1);
        break;
    }
    return chunkedList;
  },

  splitIntoCategories(list) {
    let splittedList = [];
    var i;
    for (i = 0; i < 3; i++) {
      splittedList.push([]);
    }

    list.forEach(note => {
      if (note.deleted) {
        splittedList[2].push(note);
      } else if (note.archived) {
        splittedList[1].push(note);
      } else {
        splittedList[0].push(note);
      }
    });

    return splittedList;
  }
};
