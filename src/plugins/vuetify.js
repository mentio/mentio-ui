import Vue from "vue";
import Vuetify from "vuetify/lib";
import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      dark: {
        primary: "#3f51b5",
        secondary: "#607d8b",
        accent: "#3d5afe",
        error: "#ff5722",
        warning: "#ffc107",
        info: "#03a9f4",
        success: "#4caf50",
        note_default: "#424242",
        note_grey: "#455A64",
        note_yellow: "#827717",
        note_orange: "#E65100",
        note_light_green: "#558B2F",
        note_dark_green: "#1B5E20",
        note_light_blue: "#0288D1",
        note_türkis: "#0097A7",
        note_purple: "#4527A0",
        note_blue: "#1565C0",
        note_red: "#D84315",
        note_pink: "#C2185B"
      },
      light: {
        primary: "#3f51b5",
        secondary: "#607d8b",
        accent: "#3d5afe",
        error: "#ff5722",
        warning: "#ffc107",
        info: "#03a9f4",
        success: "#4caf50",
        note_default: "#FFFFFF",
        note_grey: "#CFD8DC",
        note_yellow: "#FFF59D",
        note_orange: "#FFCC80",
        note_light_green: "#C5E1A5",
        note_dark_green: "#9CCC65",
        note_light_blue: "#B3E5FC",
        note_türkis: "#80CBC4",
        note_purple: "#D1C4E9",
        note_blue: "#90CAF9",
        note_red: "#EF9A9A",
        note_pink: "#F8BBD0"
      }
      /*themes: {
      light: {
        primary: "#3f51b5",
        secondary: "#607d8b",
        accent: "#3d5afe",
        qrCodeError: "#ff5722",
        warning: "#ffc107",
        info: "#03a9f4",
        success: "#4caf50"
      }*/
    }
  },
  icons: {
    iconfont: "mdi"
  }
});
