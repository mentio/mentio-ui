import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
import Home from "../views/Home.vue";
import Login from "../views/Login";
import Signup from "../views/Signup";
import ArchivedNotes from "../views/ArchivedNotes";
import DeletedNotes from "../views/DeletedNotes";
import Label from "../views/Label";
import Search from "../views/Search";
import Navbar from "../components/Navbar";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    components: {
      default: Home,
      navbar: Navbar
    }
  },
  {
    path: "/archive",
    name: "archive",
    components: {
      default: ArchivedNotes,
      navbar: Navbar
    }
  },
  {
    path: "/trash",
    name: "trash",
    components: {
      default: DeletedNotes,
      navbar: Navbar
    }
  },
  {
    path: "/label/:name",
    components: {
      default: Label,
      navbar: Navbar
    }
  },
  {
    path: "/search",
    components: {
      default: Search,
      navbar: Navbar
    }
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup
  },
  {
    path: "/settings",
    name: "settings",
    components: {
      default: () =>
        import(/* webpackChunkName: "about" */ "../views/Settings.vue"),
      navbar: Navbar
    }
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "*",
    redirect: "/"
  }
];

const router = new VueRouter({
  routes
});

export default router;

const routeGuard = async (to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  // disabled forced login
  /*
  const publicPages = ["/login", "/signup"];
  const authRequired = !publicPages.includes(to.path);
  let loggedIn = store.getters["user/loggedIn"];
  if (authRequired && !loggedIn) {
    return next("/login");
  }
  */
  await store.restored;
  next();
};

router.beforeEach(routeGuard);
