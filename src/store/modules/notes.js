/* eslint-disable no-console */

export default {
  namespaced: true,

  state: {
    notes: [],
    labels: []
  },

  getters: {
    activeNotes: state => {
      return state.notes.filter(note => !note.archived && !note.deleted);
    },
    archivedNotes: state => {
      return state.notes.filter(note => note.archived && !note.deleted);
    },
    deletedNotes: state => {
      return state.notes.filter(note => note.deleted);
    }
  },

  mutations: {
    addNote(state, newNote) {
      state.notes.unshift(newNote);
    },
    removeNote(state, id) {
      let targetIndex = state.notes.map(note => note.id).indexOf(id);
      state.notes.splice(targetIndex, 1);
    },
    deleteNote(state, id) {
      let targetIndex = state.notes.map(note => note.id).indexOf(id);
      state.notes[targetIndex].deleted = true;
    },
    restoreNote(state, id) {
      let targetIndex = state.notes.map(note => note.id).indexOf(id);
      state.notes[targetIndex].deleted = false;
      state.notes[targetIndex].archived = false;
    },
    archiveNote(state, id) {
      let targetIndex = state.notes.map(note => note.id).indexOf(id);
      state.notes[targetIndex].archived = true;
    },
    unarchiveNote(state, id) {
      let targetIndex = state.notes.map(note => note.id).indexOf(id);
      state.notes[targetIndex].archived = false;
    },
    changeNote(state, payload) {
      let targetIndex = state.notes.map(note => note.id).indexOf(payload.id);
      payload.changedNote.items = payload.changedNote.items.filter(
        item => item.text !== ""
      );
      state.notes[targetIndex] = payload.changedNote;
    },
    updateColorOfNote(state, payload) {
      let targetIndex = state.notes.map(note => note.id).indexOf(payload.id);
      state.notes[targetIndex].color = payload.color;
    },
    clearNotes(state) {
      state.notes = [];
    },
    addLabelToNote(state, payload) {
      let targetIndex = state.notes.map(note => note.id).indexOf(payload.id);
      if (
        state.notes[targetIndex].labels
          .map(label => label.id)
          .includes(payload.label.id)
      ) {
        return;
      }
      if (
        state.labels.map(label => label.id).indexOf(payload.label.id) === -1
      ) {
        return;
      }
      state.notes[targetIndex].labels.push(payload.label);
    },
    removeLabelFromNote(state, payload) {
      let noteTargetIndex = state.notes
        .map(note => note.id)
        .indexOf(payload.noteId);
      let labelTargetIndex = state.notes[noteTargetIndex].labels
        .map(label => label.id)
        .indexOf(payload.labelId);
      state.notes[noteTargetIndex].labels.splice(labelTargetIndex, 1);
    },
    addLabel(state, label) {
      label.name = label.name.trim();
      if (label.name === "") {
        return;
      }
      if (state.labels.map(label => label.id).indexOf(label.id) !== -1) {
        return;
      }
      if (state.labels.map(label => label.name).indexOf(label.name) !== -1) {
        return;
      }
      state.labels.push(label);
    },
    removeLabel(state, id) {
      let targetIndex = state.labels.map(label => label.id).indexOf(id);
      state.labels.splice(targetIndex, 1);

      state.notes.forEach(note => {
        let indexedLabels = note.labels.map(label => label.id);
        let targetIndex = indexedLabels.indexOf(id);
        if (targetIndex !== -1) {
          note.labels.splice(targetIndex, 1);
        }
      });
    },
    updateLabel(state, label) {
      let targetIndex = state.labels.map(label => label.id).indexOf(label.id);
      state.labels[targetIndex] = label;

      state.notes.forEach(note => {
        let indexedLabels = note.labels.map(label => label.id);
        let targetIndex = indexedLabels.indexOf(label.id);
        if (targetIndex !== -1) {
          note.labels[targetIndex] = label;
          console.log("update calculated");
        }
      });
    }
  },

  actions: {
    insertNewNote({ commit }, note) {
      commit("addNote", note);
    },
    deleteNote({ commit }, id) {
      commit("deleteNote", id);
    },
    deleteForeverNote({ commit }, id) {
      commit("removeNote", id);
    },
    restoreNote({ commit }, id) {
      commit("restoreNote", id);
    },
    archiveNote({ commit }, id) {
      commit("archiveNote", id);
    },
    unarchiveNote({ commit }, id) {
      commit("unarchiveNote", id);
    },
    changeNote({ commit }, payload) {
      commit("changeNote", payload);
    },
    updateColorOfNote({ commit }, payload) {
      commit("updateColorOfNote", payload);
    },
    addLabelToNote({ commit }, payload) {
      commit("addLabelToNote", payload);
    },
    removeLabelFromNote({ commit }, payload) {
      commit("removeLabelFromNote", payload);
    },
    clearNotes({ commit }) {
      commit("clearNotes");
    },
    addLabel({ commit }, label) {
      commit("addLabel", label);
    },
    removeLabel({ commit }, id) {
      commit("removeLabel", id);
    },
    updateLabel({ commit }, payload) {
      commit("updateLabel", payload);
    }
  }
};
