/* eslint-disable no-console */
import router from "../../router";

export default {
  namespaced: true,

  state: {
    tokenPair: null,
    currentUser: null,
    guestMode: true
  },

  getters: {
    loggedIn: state => {
      return (
        state.tokenPair !== null &&
        state.tokenPair.hasOwnProperty("accessToken")
      );
    }
  },

  mutations: {
    setTokenPair(state, tokenPair) {
      state.tokenPair = tokenPair;
    },
    deleteTokenPair(state) {
      state.tokenPair = "";
    },
    setCurrentUser(state, currentUser) {
      state.guestMode = false;
      state.currentUser = currentUser;
    },
    deleteCurrentUser(state) {
      state.guestMode = true;
      state.currentUser = "";
    }
  },

  actions: {
    insertNewTokenPair({ commit }, payload) {
      let accessToken = payload.access_token.token_string;
      let refreshToken = payload.refresh_token.token_string;

      var newTokenPair = {
        accessToken: accessToken,
        refreshToken: refreshToken
      };
      commit("setTokenPair", newTokenPair);
    },
    insertUserInfo({ commit }, payload) {
      commit("setCurrentUser", payload);
    },
    doLogout({ commit }) {
      commit("deleteTokenPair");
      commit("deleteCurrentUser");
      commit("notes/clearNotes", {}, { root: true });
      router.push("login");
    }
  }
};
