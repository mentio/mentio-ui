/* eslint-disable no-console */

export default {
  namespaced: true,

  state: {
    modifyDialogActive: false,
    gridViewActive: true,
    searchInput: ""
  },

  mutations: {
    setModifyDialogActive(state, mode) {
      state.modifyDialogActive = mode;
    },
    setGridViewActive(state, mode) {
      state.gridViewActive = mode;
    },
    setSearchInput(state, input) {
      console.log(input);
      state.searchInput = input;
    }
  },

  actions: {
    setModifyDialogActive({ commit }, mode) {
      commit("setModifyDialogActive", mode);
    },
    setGridViewActive({ commit }, mode) {
      commit("setGridViewActive", mode);
    },
    setSearchInput({ commit }, input) {
      commit("setSearchInput", input);
    }
  }
};
