import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import user from "./modules/user";
import notes from "./modules/notes";
import settings from "./modules/settings";
import uistates from "./modules/uistates";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ["user", "notes", "settings"]
});

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    notes,
    settings,
    uistates
  },
  plugins: [vuexLocal.plugin]
});

export default store;
