import axios from "axios";

export default {
  signup(payload) {
    // eslint-disable-next-line no-console
    console.log(axios.defaults.baseURL);
    return axios.post("/signup", payload);
  },

  login(payload) {
    // eslint-disable-next-line no-console
    console.log(axios.defaults.baseURL);

    // eslint-disable-next-line no-console
    console.log(process.env.VUE_APP_USER_API_BASE_URL);
    return axios.post("/login", payload).then(response => {
      return response.data;
    });
  },

  refresh(payload) {
    return axios.post("/refresh", payload).then(response => {
      return response.data;
    });
  }
};

axios.defaults.baseURL = process.env.VUE_APP_USER_API_BASE_URL;
