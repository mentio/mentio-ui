export default {
  generateID() {
    return Math.random()
      .toString(36)
      .substr(2, 9);
  }
};
